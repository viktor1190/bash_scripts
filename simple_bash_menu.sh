#!/bin/bash

# COMIENZO DE PROGRAMA
while true; do
	printf "
Seleccione una opción:
1 %s
2 %s
3 %s\n\n
" "opcion 1" "opcion 2" "salir"

	read n
	case $n in
	    1) echo seleccionado1;;
	    2) echo seleccionado2;;
	    3) exit;;
	    *) echo invalid option;;
	esac

	# otra forma de hacer la lectura sin usar read :
	#select op in "1" "2" "3"; do
	#	case $op in
	#		1 ) echo seleccionado1;;
	#		2 ) echo seleccionado2;;
	#		3 ) break;;
	#	esac
	#done
done